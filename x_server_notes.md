heroku pgbackups:capture --expire --app myedify
heroku pgbackups:restore DATABASE `heroku pgbackups:url --app myedify` --app edify-staging

## LOCAL IMPORT
pg_restore --verbose --clean --no-acl --no-owner -h localhost -U postgres -d myedify bNNN.dump

Deployment Steps:

1. Turn on Maintenance Mode: heroku maintenance:on -a myedify
2. Push git repo: git push production master
3. Turn off Maintenance Mode: heroku maintenance:off -a myedify

heroku maintenance:on -a myedify
git push production master
heroku maintenance:off -a myedify

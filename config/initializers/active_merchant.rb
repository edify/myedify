if Rails.env == 'development'
  EdifyDonor::Application.config.after_initialize do
    ActiveMerchant::Billing::Base.mode = :test
    ::GATEWAY = ActiveMerchant::Billing::FirstdataE4Gateway.new(
      :login     => 'AD1242-06',
      :password  => 'x1oazl2v'
    )
  end
elsif Rails.env == 'test'
  EdifyDonor::Application.config.after_initialize do
    ActiveMerchant::Billing::Base.mode = :test
    ::GATEWAY = ActiveMerchant::Billing::FirstdataE4Gateway.new(
      :login     => 'AD1242-06',
      :password  => 'x1oazl2v'
    )
  end
elsif Rails.env == 'production'
  EdifyDonor::Application.config.after_initialize do
    ActiveMerchant::Billing::Base.mode = :live
    ::GATEWAY = ActiveMerchant::Billing::FirstdataE4Gateway.new(
      :login     => 'A30419-01',
      :password  => '1i4233hr'
    )
  end

# live info:
      # :login     => 'A30419-01',
      # :password  => '1i4233hr'

end
EdifyDonor::Application.routes.draw do

  resources :categories, :except => [:index, :show]
#   resources :forums, :except => :index do
#     resources :topics, :shallow => true, :except => :index do
#       resources :posts, :shallow => true, :except => [:index, :show]
#     end
#     root to: 'categories#index', via: :get
#   end

  authenticated :user do
#     root :to => 'home#dashboard'
    get 'home/list_schools', to: 'home#list_schools'
    get 'schools', to: 'home#list_schools'
    get 'home/show_schools', to: 'home#show_school'
    get 'home/show_schools/:id', to: 'home#show_school'
    get 'school/:id', to: 'home#show_school'
    get 'home/map_schools', to: 'home#map_schools'
    get 'map', to: 'home#map_schools'
    get 'home/dashboard', to: 'home#dashboard'
    get 'home/prayer_list', to: 'home#prayer_list'
    get 'home/prayer_feed', to: 'home#prayer_feed'
    get 'home/school_prayer', to: 'home#school_prayer'
    get 'home/school_visited', to: 'home#school_visited'
    get 'home/want_to_visit_school', to: 'home#want_to_visit_school'
    get 'home/school_visit_tour', to: 'home#school_visit_tour'
    get 'home/virtual_trip', to: 'home#virtual_trip'
    get 'home/tour', to: 'home#tour'
    get 'home/create_prayer_request', to: 'home#create_prayer_request'
    get 'home/pray_for_user/:id', to: 'home#pray_for_user'
    get 'home/create_school_note', to: 'home#create_school_note'
    get 'home/impact_portfolio', to: 'home#impact_portfolio'
    get 'home/schools_visited', to: 'home#schools_visited'
    get 'home/recently_added', to: 'home#recently_added'
    get 'home/tour_toggle', to: 'home#tour_toggle'
    get 'home/my_fundraisers', to: 'home#my_fundraisers'

    # get 'fundraisers/new', to: 'fundraisers#new'
    # get 'fundraisers/edit', to: 'fundraisers#edit'


    get 'home/school_info_window/:id', to: 'home#school_info_window'

#     if Rails.env.production?
# #       scope :protocol => 'http://', :constraints => { :protocol => 'http://', :domain => "edify-donor.herokuapp.com" } do
# #         resources :donations, :constraints => { :protocol => 'http://', :domain => "edify-donor.herokuapp.com" }
# #       end
#       get 'donations/new', to: redirect("http://edify-donor.herokuapp.com/donations/new"), :constraints => { :protocol => "http://" }
#     end
    get 'donations/fundraiser', to: 'donations#fundraiser'
    get 'donations/create_fundraiser_donation', to: 'donations#create_fundraiser_donation'

    resources :donations
    get 'fundraisers/list', to: 'fundraisers#list'

    resources :fundraisers
  end
  
  get 'iframe_map', to: 'home#iframe_map'
  get 'iframe_school_info_window/:id', to: 'home#iframe_school_info_window'
  
  get 'fundraisers/list', to: 'fundraisers#list'
  get 'fundraisers', to: 'fundraisers#index'
  get 'fundraisers/:id', to: 'fundraisers#show'
  get 'donations/fundraiser', to: 'donations#fundraiser'
  get 'donations/create_fundraiser_donation', to: 'donations#create_fundraiser_donation'

  root :to => "home#index"
  devise_for :users
  resources :users


end

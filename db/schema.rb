# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150724050631) do

  create_table "categories", :force => true do |t|
    t.string   "title"
    t.boolean  "state",      :default => true
    t.integer  "position",   :default => 0
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "donation_transactions", :force => true do |t|
    t.integer  "donation_id"
    t.string   "action"
    t.integer  "amount"
    t.boolean  "success"
    t.string   "authorization"
    t.string   "message"
    t.text     "params"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "donations", :force => true do |t|
    t.integer  "user_id"
    t.integer  "amount_cents"
    t.string   "billing_first"
    t.string   "billing_last"
    t.string   "billing_address"
    t.string   "billing_city"
    t.string   "billing_state"
    t.string   "billing_zip"
    t.string   "ip_address"
    t.string   "card_brand"
    t.date     "card_expires_on"
    t.datetime "donated_at"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.float    "total_amount"
    t.integer  "fundraiser_id"
  end

  create_table "forums", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "state",        :default => true
    t.integer  "topics_count", :default => 0
    t.integer  "posts_count",  :default => 0
    t.integer  "position",     :default => 0
    t.integer  "category_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "fundraisers", :force => true do |t|
    t.integer  "user_id"
    t.string   "friendly_url"
    t.string   "name"
    t.integer  "goal_amount_cents"
    t.string   "category"
    t.text     "motivation"
    t.integer  "time_in_days"
    t.date     "end_date"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "headline"
    t.string   "photo_url"
  end

  create_table "posts", :force => true do |t|
    t.text     "body"
    t.integer  "forum_id"
    t.integer  "topic_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "school_prayers", :force => true do |t|
    t.integer  "school_id"
    t.integer  "user_id"
    t.string   "school_name"
    t.string   "proprietor_first"
    t.string   "proprietor_last"
    t.string   "region"
    t.string   "country"
    t.float    "lat"
    t.float    "lng"
    t.string   "photo_file_name"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.boolean  "has_actual_coordinates"
    t.integer  "enrollment"
    t.string   "enrollment_data_source"
  end

  create_table "school_prayers_users", :id => false, :force => true do |t|
    t.integer "school_prayer_id"
    t.integer "user_id"
  end

  add_index "school_prayers_users", ["school_prayer_id", "user_id"], :name => "index_school_prayers_users_on_school_prayer_id_and_user_id"
  add_index "school_prayers_users", ["user_id"], :name => "index_school_prayers_users_on_user_id"

  create_table "school_visits", :force => true do |t|
    t.integer  "school_id"
    t.integer  "user_id"
    t.string   "school_name"
    t.string   "proprietor_first"
    t.string   "proprietor_last"
    t.string   "region"
    t.string   "country"
    t.float    "lat"
    t.float    "lng"
    t.string   "photo_file_name"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.boolean  "has_actual_coordinates"
    t.integer  "enrollment"
    t.string   "enrollment_data_source"
  end

  create_table "school_visits_users", :id => false, :force => true do |t|
    t.integer "school_visit_id"
    t.integer "user_id"
  end

  add_index "school_visits_users", ["school_visit_id", "user_id"], :name => "index_school_visits_users_on_school_visit_id_and_user_id"
  add_index "school_visits_users", ["user_id"], :name => "index_school_visits_users_on_user_id"

  create_table "topics", :force => true do |t|
    t.string   "title"
    t.integer  "hits",        :default => 0
    t.boolean  "sticky",      :default => false
    t.boolean  "locked",      :default => false
    t.integer  "posts_count"
    t.integer  "forum_id"
    t.integer  "user_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "user_actions", :force => true do |t|
    t.integer  "user_id"
    t.integer  "school_id"
    t.integer  "to_user_id"
    t.text     "content"
    t.string   "action_type"
    t.float    "lat"
    t.float    "lng"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "city"
    t.string   "state"
    t.string   "state_code"
    t.string   "postal_code"
    t.string   "country"
    t.string   "country_code"
    t.string   "school_name"
    t.string   "school_photo_file_name"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",   :null => false
    t.string   "encrypted_password",     :default => "",   :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "name"
    t.integer  "topics_count",           :default => 0
    t.integer  "posts_count",            :default => 0
    t.boolean  "show_tour",              :default => true
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar_url"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "users_want_to_visit_schools", :id => false, :force => true do |t|
    t.integer "want_to_visit_school_id"
    t.integer "user_id"
  end

  add_index "users_want_to_visit_schools", ["user_id"], :name => "index_users_want_to_visit_schools_on_user_id"
  add_index "users_want_to_visit_schools", ["want_to_visit_school_id", "user_id"], :name => "users_want_visit_school"

  create_table "want_to_visit_schools", :force => true do |t|
    t.integer  "school_id"
    t.integer  "user_id"
    t.string   "school_name"
    t.string   "proprietor_first"
    t.string   "proprietor_last"
    t.string   "region"
    t.string   "country"
    t.float    "lat"
    t.float    "lng"
    t.string   "photo_file_name"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.boolean  "has_actual_coordinates"
    t.integer  "enrollment"
    t.string   "enrollment_data_source"
  end

end

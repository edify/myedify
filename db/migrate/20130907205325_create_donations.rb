class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.integer :user_id
      t.integer :amount_cents
      t.string :billing_first
      t.string :billing_last
      t.string :billing_address
      t.string :billing_city
      t.string :billing_state
      t.string :billing_zip
      t.string :ip_address
      t.string :card_brand
      t.date :card_expires_on
      t.datetime :donated_at
      t.timestamps
    end
  end
end

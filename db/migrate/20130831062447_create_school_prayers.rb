class CreateSchoolPrayers < ActiveRecord::Migration
  def change
    create_table :school_prayers do |t|
      t.integer :school_id
      t.integer :user_id
      t.string :school_name
      t.string :proprietor_first
      t.string :proprietor_last
      t.string :region
      t.string :country
      t.float :lat
      t.float :lng
      t.string :photo_file_name
      t.timestamps
    end    

    create_table :school_prayers_users, :id => false do |t|
      t.integer :school_prayer_id
      t.integer :user_id
    end
    add_index :school_prayers_users, [:school_prayer_id, :user_id]
    add_index :school_prayers_users, :user_id
  end
end

class AddCityStateCountryToUserAction < ActiveRecord::Migration
  def change
    add_column :user_actions, :city, :string
    add_column :user_actions, :state, :string
    add_column :user_actions, :state_code, :string
    add_column :user_actions, :postal_code, :string
    add_column :user_actions, :country, :string
    add_column :user_actions, :country_code, :string
    add_column :user_actions, :school_name, :string
    add_column :user_actions, :school_photo_file_name, :string
  end
end
class AddPhotosOtherFields < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :avatar_url, :string
    add_column :fundraisers, :headline, :string
    add_column :fundraisers, :photo_url, :string
  end
end

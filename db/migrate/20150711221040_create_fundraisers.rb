class CreateFundraisers < ActiveRecord::Migration
  def change
    create_table :fundraisers do |t|
      t.integer :user_id
      t.string :friendly_url
      t.string :name
      t.integer :goal_amount_cents
      t.string :category
      t.text :motivation
      t.integer :time_in_days
      t.date :end_date

      t.timestamps
    end
    
    add_column :donations, :fundraiser_id, :integer
  end
end

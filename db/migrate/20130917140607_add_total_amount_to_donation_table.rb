class AddTotalAmountToDonationTable < ActiveRecord::Migration
  def change
    add_column :donations, :total_amount, :float
  end
end

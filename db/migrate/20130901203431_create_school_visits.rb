class CreateSchoolVisits < ActiveRecord::Migration
  def change
    create_table :school_visits do |t|
      t.integer :school_id
      t.integer :user_id
      t.string :school_name
      t.string :proprietor_first
      t.string :proprietor_last
      t.string :region
      t.string :country
      t.float :lat
      t.float :lng
      t.string :photo_file_name
      t.timestamps
    end    

    create_table :school_visits_users, :id => false do |t|
      t.integer :school_visit_id
      t.integer :user_id
    end
    add_index :school_visits_users, [:school_visit_id, :user_id]
    add_index :school_visits_users, :user_id
  end
end
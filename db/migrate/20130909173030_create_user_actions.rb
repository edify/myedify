class CreateUserActions < ActiveRecord::Migration
  def change
    create_table :user_actions do |t|
      t.integer :user_id
      t.integer :school_id
      t.integer :to_user_id
      t.text :content
      t.string :action_type
      t.float :lat
      t.float :lng     
      t.timestamps
    end
  end
end

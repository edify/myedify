class AddEnrollmentDataToModels < ActiveRecord::Migration
  def change
    add_column :want_to_visit_schools, :has_actual_coordinates, :boolean
    add_column :want_to_visit_schools, :enrollment, :integer
    add_column :want_to_visit_schools, :enrollment_data_source, :string

    add_column :school_visits, :has_actual_coordinates, :boolean
    add_column :school_visits, :enrollment, :integer
    add_column :school_visits, :enrollment_data_source, :string
    
    add_column :school_prayers, :has_actual_coordinates, :boolean
    add_column :school_prayers, :enrollment, :integer
    add_column :school_prayers, :enrollment_data_source, :string
  end
end

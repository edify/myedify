class CreateWantToVisitSchools < ActiveRecord::Migration
  def change
    create_table :want_to_visit_schools do |t|
      t.integer :school_id
      t.integer :user_id
      t.string :school_name
      t.string :proprietor_first
      t.string :proprietor_last
      t.string :region
      t.string :country
      t.float :lat
      t.float :lng
      t.string :photo_file_name
      t.timestamps
    end    

    create_table :users_want_to_visit_schools, :id => false do |t|
      t.integer :want_to_visit_school_id
      t.integer :user_id
    end
    add_index :users_want_to_visit_schools, [:want_to_visit_school_id, :user_id], :name => 'users_want_visit_school'
    add_index :users_want_to_visit_schools, :user_id
  end
end

# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :donation do
    user_id "MyString"
    transaction_id "MyString"
    amount_in_cents 1
    billing_address "MyString"
    billing_city "MyString"
    billing_state "MyString"
    billing_zip "MyString"
    transaction_result "MyString"
  end
end

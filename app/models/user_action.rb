class UserAction < ActiveRecord::Base
  attr_accessible :school_id, :user_id, :to_user_id, :content, :action_type, :lat, :lng, :city, :state, :state_code, :postal_code
  attr_accessible :country, :country_code, :school_name, :school_photo_file_name
  belongs_to :user
  reverse_geocoded_by :lat, :lng
  
  default_scope { order('created_at DESC') }
end

class Donation < ActiveRecord::Base
  belongs_to :user
  attr_accessible :total_amount, :amount_cents, :billing_address, :billing_city, :billing_state, :billing_zip
  attr_accessible :transaction_id, :transaction_result, :user_id, :amount_cents
  attr_accessible :billing_first, :billing_last, :card_brand, :card_expires_on
  attr_accessible :card_number, :card_verification
  attr_accessor :card_number, :card_verification

  has_many :transactions, :class_name => "DonationTransaction"
  belongs_to :fundraiser

  validate :validate_card, :on => :create

  def donate
    response = GATEWAY.purchase(donation_in_cents, credit_card, purchase_options)
    transactions.create!(:action => "purchase", :amount => donation_in_cents, :response => response)
    self.update_attribute(:donated_at, Time.now) if response.success?
    response.success?
  end

  def donation_in_cents
    amount_cents
  end

  private

  def purchase_options
    {
      :ip => ip_address,
      :billing_address => {
        :name     => billing_first+billing_last,
        :address1 => billing_address,
        :city     => billing_city,
        :state    => billing_state,
        :country  => "US",
        :zip      => billing_zip
      }
    }
  end

  def validate_card
    unless credit_card.valid?
      credit_card.errors.full_messages.each do |message|
        errors.add :base, "message"
      end
    end
  end

  def credit_card
    @credit_card ||= ActiveMerchant::Billing::CreditCard.new(
      :brand              => card_brand,
      :number             => card_number,
      :verification_value => card_verification,
      :month              => card_expires_on.month,
      :year               => card_expires_on.year,
      :first_name         => billing_first,
      :last_name          => billing_last
    )
  end

end

class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :role_ids, :as => :admin
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :show_tour, :first_name, :last_name, :avatar_url

  has_and_belongs_to_many :school_prayers, :uniq => true
  has_and_belongs_to_many :school_visits, :uniq => true
  has_and_belongs_to_many :want_to_visit_schools, :uniq => true

  has_many :user_actions

  has_many :donations
  has_many :fundraisers

  has_many :topics, :dependent => :destroy
  has_many :posts, :dependent => :destroy

  after_create :show_them_the_tour

  def username
    name
  end

  def show_them_the_tour
    self.show_tour = true
    save
  end
end

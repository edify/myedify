class Fundraiser < ActiveRecord::Base
  attr_accessible :category, :end_date, :friendly_url, :goal_amount_cents, :motivation, :name, :time_in_days, :user_id, :photo_url, :headline
  
  belongs_to :user
  has_many :donations
end

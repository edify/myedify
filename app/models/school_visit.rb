class SchoolVisit < ActiveRecord::Base
  attr_accessible :school_id, :user_id, :school_name, :proprietor_first, :proprietor_last, :region, 
                  :country, :lat, :lng, :photo_file_name, :has_actual_coordinates, :enrollment, :enrollment_data_source
  has_and_belongs_to_many :users, :uniq => true
end

class FundraisersController < ApplicationController
  # GET /fundraisers
  # GET /fundraisers.json
  def index
    @fundraisers = Fundraiser.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @fundraisers }
    end
  end

  def list
    @fundraisers = Fundraiser.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @fundraisers }
    end
  end

  # GET /fundraisers/1
  # GET /fundraisers/1.json
  def show
    @fundraiser = Fundraiser.where(:friendly_url => params[:id]).first
    if @fundraiser.blank?
      @fundraiser = Fundraiser.find(params[:id])
    end

    @donations = Donation.where(fundraiser_id: @fundraiser.id).order('donated_at DESC').all
    @amount_raised = Donation.where(fundraiser_id: @fundraiser.id).sum :total_amount
    @percent_complete = (@amount_raised.to_f / (@fundraiser.goal_amount_cents / 100).to_f)*100.to_i

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @fundraiser }
    end
  end

  # GET /fundraisers/new
  # GET /fundraisers/new.json
  def new
    @s3_direct_post = S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", success_action_status: 201, acl: :public_read)
    @fundraiser = Fundraiser.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @fundraiser }
    end
  end

  # GET /fundraisers/1/edit
  def edit
    @fundraiser = Fundraiser.find(params[:id])
  end

  # POST /fundraisers
  # POST /fundraisers.json
  def create
    @fundraiser = Fundraiser.new(params[:fundraiser])
    @fundraiser.user_id = current_user.id
    @fundraiser.goal_amount_cents = params[:goal_amount].to_i * 100

    respond_to do |format|
      if @fundraiser.save
        format.html { redirect_to @fundraiser, notice: 'Fundraiser was successfully created.' }
        format.json { render json: @fundraiser, status: :created, location: @fundraiser }
      else
        format.html { render action: "new" }
        format.json { render json: @fundraiser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /fundraisers/1
  # PUT /fundraisers/1.json
  def update
    @fundraiser = Fundraiser.find(params[:id])

    respond_to do |format|
      if @fundraiser.update_attributes(params[:fundraiser])
        format.html { redirect_to @fundraiser, notice: 'Fundraiser was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @fundraiser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fundraisers/1
  # DELETE /fundraisers/1.json
  def destroy
    @fundraiser = Fundraiser.find(params[:id])
    @fundraiser.destroy

    respond_to do |format|
      format.html { redirect_to fundraisers_url }
      format.json { head :no_content }
    end
  end
end

class DonationsController < ApplicationController
require 'pp'
require 'rubygems'
require 'active_merchant'
require 'json'
  # GET /donations
  # GET /donations.json
  def index
    @donations = Donation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @donations }
    end
  end

  # GET /donations/1
  # GET /donations/1.json
  def show
    @donation = Donation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @donation }
    end
  end

  # GET /donations/new
  # GET /donations/new.json
  def new
    @donation = Donation.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @donation }
    end
  end

  def fundraiser
    @donation = Donation.new
    @fundraiser = Fundraiser.where(id: params[:fundraiser_id].to_i).first
    @gift_amount = params[:gift_amount]
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @donation }
    end
  end

  # GET /donations/1/edit
  def edit
    @donation = Donation.find(params[:id])
  end

  # POST /donations
  # POST /donations.json
  def create    
  @donation = Donation.new(params[:donation])
  @donation.amount_cents = (params[:donation][:total_amount].to_f * 100)
  @donation.ip_address = request.remote_ip
  @donation.user_id = current_user.id

    respond_to do |format|
      if @donation.save
          if @donation.donate
            format.html do
              @transaction = DonationTransaction.where(donation_id: @donation.id)
              @ctr = @transaction.first.params["ctr"].sub("Purchase", "Donation")
              UserMailer.donation_receipt(current_user,@donation,@ctr).deliver
              UserMailer.donation_staff_notification(current_user,@donation,@ctr).deliver
              render action: "thanks", notice: 'Donation was successful.'
            end
            format.json { render json: @donation, status: :created, location: @donation }
          else
            render :action => "try_again", notice: 'Donation was not successful.'
          end
      else
        format.html { render action: "new" }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /donations
  # POST /donations.json
  def create_fundraiser_donation    
  @donation = Donation.new(params[:donation])
  @donation.amount_cents = (params[:donation][:total_amount].to_f * 100)
  @donation.ip_address = request.remote_ip
  @donation.user_id = current_user.id unless current_user.blank?
  @donation.fundraiser_id = params[:fundraiser_id]

    respond_to do |format|
      if @donation.save
          if @donation.donate
            format.html do
              @transaction = DonationTransaction.where(donation_id: @donation.id)
              @ctr = @transaction.first.params["ctr"].sub("Purchase", "Donation")
              UserMailer.donation_receipt(current_user,@donation,@ctr).deliver
              UserMailer.donation_staff_notification(current_user,@donation,@ctr).deliver
              render action: "thanks", notice: 'Donation was successful.'
            end
            format.json { render json: @donation, status: :created, location: @donation }
          else
            render :action => "try_again", notice: 'Donation was not successful.'
          end
      else
        format.html { render action: "new" }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  def thanks
    
  end

  # PUT /donations/1
  # PUT /donations/1.json
  def update
    @donation = Donation.find(params[:id])

    respond_to do |format|
      if @donation.update_attributes(params[:donation])
        format.html { redirect_to @donation, notice: 'Donation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /donations/1
  # DELETE /donations/1.json
  def destroy
    @donation = Donation.find(params[:id])
    @donation.destroy

    respond_to do |format|
      format.html { redirect_to donations_url }
      format.json { head :no_content }
    end
  end

end
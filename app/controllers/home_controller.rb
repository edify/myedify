class HomeController < ApplicationController

  include ApplicationHelper
  include HomeHelper

  require 'pp'
  require 'uri'
  require 'open-uri'
  require 'openssl'
  require 'json'
  require 's3'

  def index
    if Rails.env.development?
      @newest_four = JSON.parse(open("http://localhost:5000/api/home_data/newest_four?token=784c7d6a5e6af6f24cba13e636ae506e").read)
      pp @newest_four
    else
      @newest_four = SchoolDbNewestFour.cached_request
    end
    render layout: "index"
  end

  def dashboard
    if Rails.env.development?
      @newest_four = JSON.parse(open("http://localhost:5000/api/home_data/newest_four?token=784c7d6a5e6af6f24cba13e636ae506e").read)
    else
      @newest_four = SchoolDbNewestFour.cached_request
    end
    @recent_prayer_for_donor = UserAction.where(:to_user_id => current_user.id).where("created_at > ?", 6.weeks.ago).last
  end

  def prayer_list
    @prayer_list = current_user.school_prayers.all
  end

  def prayer_feed
    @prayer_list = SchoolPrayer.all
  end

  def virtual_trip
    @school_wishlist = current_user.want_to_visit_schools.all.group_by(&:country)
    pp @school_wishlist
  end

  def impact_portfolio
    @donations = Donation.where(user_id: current_user.id)
  end

  def schools_visited
    @schools_visited = current_user.school_visits.all
  end

  def recently_added
    if Rails.env.development?
      @newest_four = JSON.parse(open("http://localhost:5000/api/home_data/newest_four?token=784c7d6a5e6af6f24cba13e636ae506e").read)
    else
      @newest_four = SchoolDbNewestFour.cached_request
    end
  end

  def my_fundraisers
    @fundraisers = Fundraiser.where(user_id: current_user.id).all
  end

  def tour_toggle
    current_user.show_tour = false
    if current_user.save
      render :json => { status: "success"}
    else
      render :json => { status: "fail"}
    end
  end

  def pray_for_user
    result = request.location
    @user_action = UserAction.create(:user_id => current_user.id,
      :to_user_id => params[:id],
      :action_type => "prayer_for_user",
      :lat => result.latitude,
      :lng => result.longitude
      )
    if @user_action
      redirect_to "/users"
    end
  end

  def create_prayer_request
    result = request.location
    if result.city.blank?
      result.city << "San Diego"
      result.state << "California"
      result.state_code << "CA"
    end
    @school = SchoolPrayer.where(school_id: params[:school_id]).first

    @user_action = UserAction.create(:school_id => params[:school_id], :user_id => current_user.id,
      :content => params[:content],
      :action_type => "prayer",
      :lat => result.latitude,
      :lng => result.longitude,
      :city => result.city,
      :state => result.state,
      :state_code => result.state_code,
      :postal_code => result.postal_code,
      :country => result.country,
      :country_code => result.country_code,
      :school_name => @school.school_name,
      :school_photo_file_name => @school.photo_file_name
      )
    if @user_action
      render :json => { status: "success", prayer_html:
                                   "<div class=\"row\">
                                        <figure class=\"col-md-1 col-md-offset-1\"><i class=\"fa fa-fire fa-4x text-muted\"></i></figure>
                                        <div class=\"span6\">
                                             <div class=\"comment_name\">Prayer</div>
                                             <div class=\"comment_date\"><i class=\"fa fa-clock-o\"></i> #{@user_action.created_at.strftime("%d %b %Y")}</div>
                                             <div class=\"the_comment\">
                                                  <p>#{@user_action.content}</p>
                                             </div>
                                        </div>
                                   </div>"
      }
    else
      render :json => { status: "fail"}
    end
  end

  def create_school_note
    result = request.location
    @user_action = UserAction.create(:school_id => params[:school_id], :user_id => current_user.id,
      :content => params[:content],
      :action_type => "school_note",
      :lat => result.latitude,
      :lng => result.longitude
      )
    if @user_action
      render :json => { status: "success" }
    else
      render :json => { status: "fail"}
    end
  end

  def map_schools
    if Rails.env.development?
      holder = JSON.parse(open("http://localhost:5000/api/gps_coords?token=784c7d6a5e6af6f24cba13e636ae506e").read)
    else
      holder = SchoolDbGpsSchools.cached_request
    end

    # I had to make my own string to send to the view.  It's a hack I need to fix now that I'm not using gmaps4rails
    @markers = String.new
    @markers = "["
    holder.each do |school|
      @markers += '{"lng" : "%s", "lat" : "%s", "id" : "%s" },' % [school["gps_lng"], school["gps_lat"], school["id"]]
    end
    @markers += '{"lng" : "", "lat" : "", "name" : "", "id" : "" }]'
  end

  def iframe_map
    response.headers.delete "X-Frame-Options"
    response.headers['Content-Security-Policy'] = "child-src 'self' https://*.edify.org http://*.edify.org"
    if Rails.env.development?
      holder = JSON.parse(open("http://localhost:5000/api/gps_coords?token=784c7d6a5e6af6f24cba13e636ae506e").read)
    else
      holder = SchoolDbGpsSchools.cached_request
    end

    # I had to make my own string to send to the view.  It's a hack I need to fix now that I'm not using gmaps4rails
    @markers = String.new
    @markers = "["
    holder.each do |school|
      @markers += '{"lng" : "%s", "lat" : "%s", "id" : "%s" },' % [school["gps_lng"], school["gps_lat"], school["id"]]
    end
    @markers += '{"lng" : "", "lat" : "", "name" : "", "id" : "" }]'
    
    render :layout => false
  end

  def list_schools
    @countries = {1=>"Ghana",2=>"Dominican Republic",3=>"Rwanda",4=>"Burkina Faso",5=>"Liberia",6=>"Peru"}
#     @countries = Hash[country_hash.map {|p| [p.id, p.name]}]
    @schools = SchoolDbAllSchools.cached_request
#     @schools = JSON.parse(open("https://edifydb.herokuapp.com/api/home_data/newest_four?token=#{EdifyDonor::Application.config.school_db_api_token}").read)

  end

  def show_school
    if Rails.env.development?
      @school = JSON.parse(open("http://localhost:5000/api/schools/#{params[:id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
      @loan_data = open("http://localhost:5000/api/loans/#{params[:id]}?token=784c7d6a5e6af6f24cba13e636ae506e")
      @enrollment = JSON.parse(open("http://localhost:5000/api/enrollment/#{params[:id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
    else
      @school = JSON.parse(open("https://edifydb.herokuapp.com/api/schools/#{params[:id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
      @loan_data = open("https://edifydb.herokuapp.com/api/loans/#{params[:id]}?token=#{EdifyDonor::Application.config.school_db_api_token}")
      @enrollment = JSON.parse(open("https://edifydb.herokuapp.com/api/enrollment/#{params[:id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
    end
    unless @loan_data.read == "null"
      if Rails.env.development?
        @loan = JSON.parse(open("http://localhost:5000/api/loans/#{params[:id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
      else
        @loan = JSON.parse(open("https://edifydb.herokuapp.com/api/loans/#{params[:id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
      end
    end

    if @loan.nil?
      @loan_category = "no_loan"
      @loan_use = "Not available"
    else
      @loan_category = @loan["use_of_funds"]
      @loan_use = @loan["use_of_funds_description"]
    end


    @school_prayer = SchoolPrayer.where(:school_id => @school["id"], :user_id => current_user.id)
    @school_visited = SchoolVisit.where(:school_id => @school["id"], :user_id => current_user.id)
    @want_to_visit_school = WantToVisitSchool.where(:school_id => @school["id"], :user_id => current_user.id)

#   http://maps.googleapis.com/maps/api/geocode/json?address=la+romana&sensor=false
    region = CGI.escape(@school['region'])
    geocode_region = JSON.parse(open("http://maps.googleapis.com/maps/api/geocode/json?address=#{region}&sensor=false").read)
    if geocode_region["status"] == "OK"
      @no_coords = geocode_region["results"][0]["geometry"]["location"]["lat"].to_s + "," + geocode_region["results"][0]["geometry"]["location"]["lng"].to_s
      @map_zoom = 7
    else
      @no_coords = "18.735693,-70.162651" # DR
      @map_zoom = 8
    end

  end

  def school_info_window
    @school = JSON.parse(open("https://edifydb.herokuapp.com/api/schools/#{params[:id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)

    respond_to do |format|
      format.js
    end
  end

  def iframe_school_info_window
    @school = JSON.parse(open("https://edifydb.herokuapp.com/api/schools/#{params[:id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)

    respond_to do |format|
      format.js
    end
  end

  def school_prayer
    puts current_user.id
    @school_prayer = SchoolPrayer.where(:school_id => params[:school_id], :user_id => current_user.id)
    if @school_prayer.blank?
      if Rails.env.development?
        school = JSON.parse(open("http://localhost:5000/api/schools/#{params[:school_id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
        enrollment = JSON.parse(open("http://localhost:5000/api/enrollment/#{params[:school_id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
      else
        school = JSON.parse(open("https://edifydb.herokuapp.com/api/schools/#{params[:school_id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
        enrollment = JSON.parse(open("https://edifydb.herokuapp.com/api/enrollment/#{params[:school_id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
      end

      current_user.school_prayers << SchoolPrayer.create(:school_id => params[:school_id], :user_id => current_user.id,
        :school_name => school["name"],
        :proprietor_first => school["proprietor_first"],
        :proprietor_last => school["proprietor_last"],
        :region => school["region"],
        :lat => school["gps_lat"],
        :lng => school["gps_lng"],
        :photo_file_name => school["photo_file_name"],
        :has_actual_coordinates => school["has_actual_coordinates"],
        :enrollment => enrollment["total"],
        :enrollment_data_source => enrollment["data_source"]
        )
      render :json => { status: "add"}
    else
      SchoolPrayer.destroy(@school_prayer)
      render :json => { status: "remove"}
    end
  end

  def school_visited
    @school_visit = SchoolVisit.where(:school_id => params[:school_id], :user_id => current_user.id)
    if @school_visit.blank?
      if Rails.env.development?
        school = JSON.parse(open("http://localhost:5000/api/schools/#{params[:school_id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
        enrollment = JSON.parse(open("http://localhost:5000/api/enrollment/#{params[:school_id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
      else
        school = JSON.parse(open("https://edifydb.herokuapp.com/api/schools/#{params[:school_id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
        enrollment = JSON.parse(open("https://edifydb.herokuapp.com/api/enrollment/#{params[:school_id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
      end

      current_user.school_visits << SchoolVisit.create(:school_id => params[:school_id], :user_id => current_user.id,
        :school_name => school["name"],
        :proprietor_first => school["proprietor_first"],
        :proprietor_last => school["proprietor_last"],
        :region => school["region"],
        :lat => school["gps_lat"],
        :lng => school["gps_lng"],
        :photo_file_name => school["photo_file_name"],
        :has_actual_coordinates => school["has_actual_coordinates"],
        :enrollment => enrollment["total"],
        :enrollment_data_source => enrollment["data_source"]
        )
      render :json => { status: "add"}
    else
      SchoolVisit.destroy(@school_visit)
      render :json => { status: "remove"}
    end
  end

  def want_to_visit_school
    @want_to_visit_school = WantToVisitSchool.where(:school_id => params[:school_id], :user_id => current_user.id)
    if @want_to_visit_school.blank?
      if Rails.env.development?
        school = JSON.parse(open("http://localhost:5000/api/schools/#{params[:school_id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
        enrollment = JSON.parse(open("http://localhost:5000/api/enrollment/#{params[:school_id]}?token=784c7d6a5e6af6f24cba13e636ae506e").read)
      else
        school = JSON.parse(open("https://edifydb.herokuapp.com/api/schools/#{params[:school_id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
        enrollment = JSON.parse(open("https://edifydb.herokuapp.com/api/enrollment/#{params[:school_id]}?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
      end

  if school["gps_lat"].nil?
    coords = Geocoder.coordinates(school["region"])
      lat = coords[0]
      lng = coords[1]
    else
      lat = school["gps_lat"],
      lng = school["gps_lng"]
    end

      current_user.want_to_visit_schools << WantToVisitSchool.create(:school_id => params[:school_id], :user_id => current_user.id,
        :school_name => school["name"],
        :proprietor_first => school["proprietor_first"],
        :proprietor_last => school["proprietor_last"],
        :region => school["region"],
        :country => school["country_name"],
        :lat => school["gps_lat"],
        :lng => school["gps_lng"],
        :photo_file_name => school["photo_file_name"],
        :has_actual_coordinates => school["has_actual_coordinates"],
        :enrollment => enrollment["total"],
        :enrollment_data_source => enrollment["data_source"]
        )
      render :json => { status: "add"}
    else
      WantToVisitSchool.destroy(@want_to_visit_school)
      render :json => { status: "remove"}
    end
  end

  def tour
    @want_to_visit_school = current_user.want_to_visit_schools.all.group_by(&:country)
    @heading = Hash.new
    school_list = Array.new

    @want_to_visit_school.each do |country, sch|
      sch.each do |school|
      school_list << school
      end
    end
      school_list.each_with_index {|school, index|
        @heading[school.id] = school.bearing_to(school[index+1])
      }

    render layout: false
  end

private

  def create_tour(user_id)

  @want_to_visit_school = current_user.want_to_visit_schools.all.group_by(&:country)

    @kml = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
    xml.kml('xmlns'=>'http://www.opengis.net/kml/2.2', 'xmlns:gx'=>'http://www.google.com/kml/ext/2.2') {
    xml.Document {
      xml.name "Edify School Visit Virtual Tour"
      xml.open "1"
        xml['gx'].Tour {
          xml.name "School Visit Virtual Tour"
            xml['gx'].Playlist {
                #fly to JFK
#                  xml['gx'].FlyTo {
#                   xml['gx'].duration "6.0"
#                   xml.LookAt {
#                     xml.longitude "-73.781993"
#                     xml.latitude "40.643772"
#                     xml.altitude "400"
#                     xml.range "1000" # in meters
#                     xml.heading "180"
#                     xml.tilt "75"
#                   }
#                 }
#
#                 xml['gx'].AnimatedUpdate {
#                   xml['gx'].duration "6.0"
#                   xml.Update {
#                     xml.targetHref
#                     xml.Change {
#                       xml.Placemark('targetId'=>"jfk") {
#                         xml['gx'].balloonVisibility "1"
#                       }
#                     }
#                   }
#                 }
#                 xml['gx'].Wait {
#                   xml['gx'].duration "8.0"
#                 }
#                 xml['gx'].AnimatedUpdate {
#                   xml['gx'].duration "0.0"
#                   xml.Update {
#                     xml.targetHref
#                     xml.Change {
#                       xml.Placemark('targetId'=>"jfk") {
#                         xml['gx'].balloonVisibility "0"
#                       }
#                     }
#                   }
#                 }

                heading_index = 0
                @want_to_visit_school.each do |country, sch|

                #fly to Santo Domingo
#                  xml['gx'].FlyTo {
#                   xml['gx'].duration "6.0"
#                   xml.LookAt {
#                     xml.longitude "-69.67389246419695"
#                     xml.latitude "18.44336725723824"
#                     xml.altitude "400"
#                     xml.range "250" # in meters
#                     xml.heading "162.1427766798929"
#                     xml.tilt "75"
#                   }
#                 }
#
#                 xml['gx'].AnimatedUpdate {
#                   xml['gx'].duration "6.0"
#                   xml.Update {
#                     xml.targetHref
#                     xml.Change {
#                       xml.Placemark('targetId'=>"dominican_airport") {
#                         xml['gx'].balloonVisibility "1"
#                       }
#                     }
#                   }
#                 }
#                 xml['gx'].Wait {
#                   xml['gx'].duration "8.0"
#                 }
#                 xml['gx'].AnimatedUpdate {
#                   xml['gx'].duration "0.0"
#                   xml.Update {
#                     xml.targetHref
#                     xml.Change {
#                       xml.Placemark('targetId'=>"dominican_airport") {
#                         xml['gx'].balloonVisibility "0"
#                       }
#                     }
#                   }
#                 }
              last_school_lng = 0

              sch.each do |school|
                heading_index += 1
                xml['gx'].FlyTo {
                  xml['gx'].duration "6.0"
#                   xml['gx'].flyToMode "smooth"
                  xml.LookAt {
                    xml.longitude school.lng
                    xml.latitude school.lat
                    xml.altitude "400"
                    if last_school_lng == school.lng
                      xml.heading rand(359)
                      xml.range rand(2500...5000) # in meters
                    else
                      xml.heading school.bearing_to(@want_to_visit_school[heading_index])
                    xml.range "2500" # in meters
                    end
                    last_school_lng = school.lng
                    xml.tilt "75"
#                     xml.altitudeMode "relative"
                  }
                }

                xml['gx'].AnimatedUpdate {
                  xml['gx'].duration "6.0"
                  xml.Update {
                    xml.targetHref
                    xml.Change {
                      xml.Placemark('targetId'=>"schoolpin#{heading_index}") {
                        xml['gx'].balloonVisibility "1"
                      }
                    }
                  }
                }
                xml['gx'].Wait {
                  xml['gx'].duration "8.0"
                }
                xml['gx'].AnimatedUpdate {
                  xml['gx'].duration "0.0"
                  xml.Update {
                    xml.targetHref
                    xml.Change {
                      xml.Placemark('targetId'=>"schoolpin#{heading_index}") {
                        xml['gx'].balloonVisibility "0"
                      }
                    }
                  }
                }
              end # end each school loop in country

                #fly to Santo Domingo
#                  xml['gx'].FlyTo {
#                    xml['gx'].duration "6.0"
#                    xml.LookAt {
#                      xml.longitude "-69.67389246419695"
#                      xml.latitude "18.44336725723824"
#                      xml.altitude "400"
#                      xml.range "250" # in meters
#                      xml.heading "162.1427766798929"
#                      xml.tilt "75"
#                    }
#                  }
              end # end each country loop
            }
        }

        xml.Folder {
          xml.name "Points and polygons"

          xml.Style.pushpin!
            xml.IconStyle {
              xml.Icon {
                xml.href "http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png"
              }
            }

          xml.Placemark('id'=>"jfk") {
          xml.name "JFK Airport, New York"
          xml.description "Your journey starts with a connection through New York City.<br/>  We recommend reading When Helping Hurts and The Beautiful Tree on your journey."
          xml.styleUrl "#pushpin"
          xml.Point {
            xml.coordinates "-73.781993,40.643772,0"
            }
          }

          xml.Placemark('id'=>"dominican_airport") {
          xml.name "Las Americas International Airport, Dominican Republic"
          xml.description "<table><tr><td><img src=\"http://www.edify.org/wp-content/uploads/2012/01/Travis-150x150.jpg\"/></td><td valign=\"top\">Upon landing in the Dominican Republic, you're greeted by our staff member, Travis Vaughan.<br/>
                          Travis, Edify's Program Director in the Dominican Republic, works to grow and improve the quality of Edify's programs in the Dominican Republic.<br/>
                          Travis will be taking you around to see Edify Partner Schools on your tour.</td></tr></table>"
          xml.styleUrl "#pushpin"
          xml.Point {
            xml.coordinates "-69.67389246419695,18.44336725723824,0"
            }
          }

          heading_index = 0
          @want_to_visit_school.each do |country, sch|
          sch.each do |school|
          heading_index += 1
          xml.Placemark('id'=>"schoolpin#{heading_index}") {
          xml.name "#{school.school_name}"
          xml.description build_school_description(school)
          xml.styleUrl "#pushpin"
          xml.Point {
            xml.coordinates "#{school.lng},#{school.lat},0" #note height is last number
            }
          }
          end
          end
        }
      }

    }

    end

    amazon = S3::Service.new(access_key_id: 'AKIAICDUARC5WTUDEAUA', secret_access_key: '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')
    bucket = amazon.buckets.find('tour-kml')
    file_name = "edify_tour_#{user_id}_#{Time.now.strftime("%Y-%m-%d-%H%M%S")}.kml"
    new_object = bucket.objects.build(file_name)
    new_object.content = @kml.to_xml
    new_object.save

    return file_name
  end

  private

  def build_school_description(school)
      school_description = '<table width="580" cellpadding="5" cellspacing="0">'
      school_description += '  <tr>'
      school_description += '    <td align="left" valign="top">'
      school_description += '      <p>'
      school_description += "        <font color=\"#666666\"><strong>#{school.country}</strong></font>"
      school_description += '        <br />'
      school_description += "        <font color=\"#999999\"></font>"
      school_description += '      </p>'
      school_description += '      <p>'
      school_description += "        <a href=\"https://www.myedify.org/home/show_schools?id=#{school.school_id}\" target=\"_blank\"><strong><font color=\"#CC3333\">View in {my}edify</font></strong></a>"
      school_description += '      </p>'
      school_description += '    </td>'
      school_description += '    <td width="10" align="left" valign="top">&nbsp;</td>'
      school_description += '    <td align="right" valign="top">'
      school_description += '      <table border="0" cellspacing="0" cellpadding="0" bgcolor="white">'
      school_description += '        <tr>'
      school_description += '          <td align="center">'
      if school.photo_file_name.nil? or school.photo_file_name.blank?
        rand_no_photo = rand(3)
        school_description += "          <img src=\"http://s3.amazonaws.com/edify-donor/img/school-no-photo-#{rand_no_photo}.jpg\" width=\"400\" height=\"300\" align=\"left\" class=\"polaroid\" /></td>"
      else
        school_description += "          <img src=\"http://s3.amazonaws.com/edifydb/small/#{school.school_id}/#{school.photo_file_name}\" width=\"400\" height=\"300\" align=\"left\" class=\"polaroid\" /></td>"
      end
      school_description += '        </tr>'
      school_description += '        <tr>'
      school_description += '          <td align="left" valign="top">'
      school_description += '            <dl>'
      school_description += '              <dt><font color="#666666"><strong>School Visit:</strong></font></dt>'
      school_description += "              <dd>This stop of our journey is at #{school.school_name}.  You'll meet the proprietor, #{school.proprietor_first}."
      school_description += "              There are currently #{school.enrollment} students enrolled.</dd>"
      school_description += '            </dl>'
      school_description += '          </td>'
      school_description += '        </tr>'
      school_description += '      </table>'
      school_description += '    </td>'
      school_description += '  </tr>'
      school_description += '  <tr>'
      school_description += '    <td colspan="3" align="right" valign="top">'
      unless school.has_actual_coordinates == true
        school_description += '    <span class="label label-warning">GPS Location Accuracy: Regional</span>'
      else
        school_description += '    <span class="label label-success">GPS Location Accuracy: Exact</span>'
      end
      school_description += '    </td>'
      school_description += '  </tr>'
      school_description += '</table>'


  return school_description

  end

end

class SchoolDbNewestFour

  def self.cached_request
    Rails.cache.fetch "newest_four", :expires_in => 1.hour do
      JSON.parse(open("https://edifydb.herokuapp.com/api/home_data/newest_four?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
    end
  end

end

class SchoolDbAllSchools

  def self.cached_request
    Rails.cache.fetch "all_schools", :expires_in => 1.hour do
      JSON.parse(open("https://edifydb.herokuapp.com/api/schools?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
    end
  end

end

class SchoolDbGpsSchools

  def self.cached_request
    Rails.cache.fetch "gps_coords", :expires_in => 1.hour do
      JSON.parse(open("https://edifydb.herokuapp.com/api/gps_coords?token=#{EdifyDonor::Application.config.school_db_api_token}").read)
    end
  end

end

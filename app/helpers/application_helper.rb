module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  def get_quote(num)

    @quote = Hash.new
    case num
      when 0; @quote = {:quote => 'Education costs money, but then so does ignorance.', :quote_by => 'Sir Claus Moser'}
      when 1; @quote = {:quote => 'Education makes people easy to lead, but difficult to drive; easy to govern, but impossible to enslave.', :quote_by => 'Henry Peter Broughan'}
      when 2; @quote = {:quote => 'A teacher affects eternity; he can never tell where his influence stops.', :quote_by => 'Henry Brooks Adams'}
      when 3; @quote = {:quote => 'The function of education is to teach one to think intensively and to think critically. Intelligence plus character - that is the goal of true education.', :quote_by => 'Martin Luther King, Jr. '}
      when 4; @quote = {:quote => 'How much better to get wisdom than gold! To get understanding is to be chosen rather than silver.', :quote_by => 'Proverbs 16:16 (ESV)'}
      when 5; @quote = {:quote => 'Instruct the wise, and they will be even wiser.  Teach the righteous, and they will learn even more.', :quote_by => 'Proverbs 9:9 (NLT)'}
      when 6; @quote = {:quote => 'Teach me good judgment and knowledge, for I believe in your commandments.', :quote_by => 'Psalm 119:66 (ESV)'}
      when 7; @quote = {:quote => 'All Scripture is breathed out by God and profitable for teaching, for reproof, for correction, and for training in righteousness.', :quote_by => '2 Timothy 3:16 (ESV)'}
      when 7; @quote = {:quote => 'Train up a child in the way he should go: and when he is old, he will not depart from it.', :quote_by => 'Proverbs 22:^ (KJV)'}
      else  @quote = {:quote => 'Education is the most powerful weapon which you can use to change the world.', :quote_by => 'Nelson Mandela'}
    end

    return @quote  
  end

  def get_profile_photo(school_id)
    if Rails.env.development?
      profile_photo = open("http://localhost:5000/api/profile_photo/#{school_id}?token=abcd").read
    else      
      profile_photo = open("https://edifydb.herokuapp.com/api/profile_photo/#{school_id}?token=#{EdifyDonor::Application.config.school_db_api_token}").read
    end
    unless profile_photo == "null"   
      return JSON.parse(profile_photo)
    else
      return nil
    end
  end

end
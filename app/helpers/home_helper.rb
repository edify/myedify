module HomeHelper

  def school_story(random,name,year_founded,environment,proprietor_first,director_first,region,proprietor_gender,enrollment,country)
  
    unknown_start = false
    if year_founded == "1980" or year_founded.blank?
      year_founded = "several years"
      unknown_start = true
    end
    
    case random
    when 0, 1
      @school_story = "You\'ll find #{proprietor_first.titlecase} at #{name.titlecase} in the #{environment} area of the #{region} region in #{country}. "
      if proprietor_gender == "female"
        @school_story += "As the busy proprietress of this school, she works hard to ensure the #{enrollment} students under her care are receiving a "
        @school_story += "high-quality, Christ-centered education."
      else
        @school_story += "As the busy proprietor of this school, he works hard to ensure the #{enrollment} students under his care are receiving a "
        @school_story += "high-quality, Christ-centered education."      
      end
    else
      if unknown_start
        if proprietor_gender == "female"
          @school_story = "For #{year_founded} now, the proprietress of #{name.titlecase}, #{proprietor_first.titlecase}, has worked to create an school where "
          @school_story += "students will come receive a high-quality, Christ-centered education in the #{region} area of #{country}."
        else
          @school_story = "For #{year_founded} now, the proprietor of #{name.titlecase}, #{proprietor_first.titlecase}, has worked to create an school where "
          @school_story += "students will come receive a high-quality, Christ-centered education in the #{region} area of #{country}."
        end
      else
        if proprietor_gender == "female"
          @school_story = "#{name.titlecase} was founded in #{year_founded}. The proprietress of this school, #{proprietor_first.titlecase}, is working hard to create a place where "
          @school_story += "students will come receive a high-quality, Christ-centered education in the #{region} area of #{country}."
        else
          @school_story = "#{name.titlecase} was founded in #{year_founded}. The proprietor of this school, #{proprietor_first.titlecase}, is working hard to create a place where "
          @school_story += "students will come receive a high-quality, Christ-centered education in the #{region} area of #{country}."
        end
      end
    end
  
    return @school_story
  end

  def use_of_funds_icon(use_of_funds)
    case use_of_funds
      when "Construction"
        return content_tag(:i, "", :class => "fa fa-building-o fa-2x fa-fw")
      when "Remodel"
        return content_tag(:i, "", :class => "fa fa-cogs fa-2x fa-fw")
      when "Materials"
        return content_tag(:i, "", :class => "fa fa-book fa-2x fa-fw")
      when "Working Capital"
        return content_tag(:i, "", :class => "fa fa-money fa-2x fa-fw")
      when "Computer Lab"
        return content_tag(:i, "", :class => "fa fa-desktop fa-2x fa-fw")
      else
        return content_tag(:i, "", :class => "fa fa-bolt fa-2x fa-fw")
    end
  end

end
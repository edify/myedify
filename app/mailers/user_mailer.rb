class UserMailer < ActionMailer::Base
  default from: "webmaster@myedify.org"
  
  def donation_receipt(user, donation, ctr)
    @user = user
    @donation = donation
    @ctr = ctr
    mail(to: @user.email, subject: 'Thank you for your donation to Edify')
  end

  def donation_staff_notification(user, donation, ctr)
    @user = user
    @donation = donation
    @ctr = ctr
    mail(to: 'rthiessen@edify.org, vfolsom@edify.org, cfenton@edify.org', subject: '**New {my}edify Donation Received**')
  end
end
